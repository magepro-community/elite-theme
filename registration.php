<?php

use Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(
    ComponentRegistrar::THEME,
    'adminhtml/MagePro/EliteTheme',
    __DIR__);
